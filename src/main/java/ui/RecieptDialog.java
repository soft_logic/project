/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package ui;

import model.Product;
import model.Reciept;
import Service.ProductService;
import Service.RecieptService;
import Service.UserService;
import java.awt.Dialog;
import java.awt.Image;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import model.Customer;
import model.RecieptDetail;
import model.User;


/**
 *
 * @author User
 */
public class RecieptDialog extends javax.swing.JDialog {
        ArrayList<Product> products;
    ProductService productService = new ProductService();
    RecieptService recieptService = new RecieptService();
    Reciept reciept;
    private String countFormatted;
    private Customer customer;
    private Dialog frame;
    private float total;
    private JTable tblReciep = null;
    private float dismax;
    
    
    
    
    /**
     * Creates new form RecieptDialog
     */
    public RecieptDialog(User currentUser, String countFormatted, float total , Reciept reciept, RecieptService recieptService, float dismax, Customer customer) {
        this.reciept = reciept;
        this.recieptService = recieptService;
        this.total = total;
        this.dismax = dismax;
        tblReciep = new JTable();
        this.reciept = reciept;
        
        initComponents();
        lblQueue.setText(countFormatted);
        String formattedDate1 = LocalDateTime.now().format(DateTimeFormatter.ofPattern("" +"dd-MM-yyyy")); //เวลาแบบไม่อัพเดตตลอด
        String formattedDate2 = LocalDateTime.now().format(DateTimeFormatter.ofPattern("" + "HH:mm")); //เวลาแบบไม่อัพเดตตลอด
        lblDate.setText(formattedDate1);
        lblTime.setText(formattedDate2);
        lblUserName.setText(""+currentUser.getName());
        
        if(customer != null){
            lblMemberName.setText("" + customer.getName());
            lblTel.setText("" + customer.getTel());
            lblPoint.setText("" + customer.getPoint());
        }else{
            lblMemberName.setText(" - " );
            lblTel.setText(" - ");
            lblPoint.setText(" - " );
        
        }
         lblSubtotal2.setText("" + total);
         lblDiscount2.setText("" + dismax);
         lblTotal2.setText("" + (total-dismax));
         
         
         // เรียกเมธอดจาก PosFrame เพื่อดึงข้อมูลจาก tblRecieptDetail
        
        
//        CustomerbyTel CustomerByTel = new CustomerbyTel(frame, customer);
//        customer = CustomerByTel.getCustomer();
//        lblMemberName.setText("" + customer.getName());
//        lblTel.setText("Tel : " + customer.getTel());
//        lblPoint.setText("Point : " + customer.getPoint());
        
        ImageIcon icon = new ImageIcon("./Logo3.png");
        Image image = icon.getImage();
        int width = image.getWidth(null);
        int height = image.getHeight(null);
        Image newImage = image.getScaledInstance(120, 120, Image.SCALE_SMOOTH);
        icon.setImage(newImage);
        lblLogo.setIcon(icon);
        
       tblReciept.setModel(new AbstractTableModel() {
            String[] header = {"Name",  "Qty", "Price", "TotalPrice"};

            @Override
            public String getColumnName(int column) {
                return header[column];
            }

            @Override
            public int getRowCount() {
                return reciept.getRecieptDetails().size();
            }

            @Override
            public int getColumnCount() {
                return 4;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ArrayList<RecieptDetail> recieptDetails = reciept.getRecieptDetails();
                RecieptDetail recieptDetail = recieptDetails.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return recieptDetail.getProductName();
                    case 1:
                        return recieptDetail.getQty();
                    case 2:
                        return recieptDetail.getProductPrice();
                    case 3:
                        return recieptDetail.getTotalPrice();
                    default:
                        throw new AssertionError();
                }

            }
        }); 
    
    }

    public void setPromptPay() {
        lblCP.setText("PromptPay");
        lblCP2.setText("" + (total-dismax));
    }
    
    public void setCash() {
        lblCP.setText("Cash");
        lblCP2.setText("" + reciept.getCash());
        lblC.setText("change");
        lblC2.setText("" + (reciept.getCash()-(total-dismax)));
    }
    

   

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        lblDcoffee = new javax.swing.JLabel();
        lblLogo = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        lblDate = new javax.swing.JLabel();
        lblTime = new javax.swing.JLabel();
        lblUserName = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblReciept = new javax.swing.JTable();
        lblTotal2 = new javax.swing.JLabel();
        lblThankYou = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        lblQueueNumber = new javax.swing.JLabel();
        lblQueue = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        lblMemberName = new javax.swing.JLabel();
        lblTel = new javax.swing.JLabel();
        lblPoint = new javax.swing.JLabel();
        lblDate1 = new javax.swing.JLabel();
        lblTime1 = new javax.swing.JLabel();
        lblUserName1 = new javax.swing.JLabel();
        lblMemberName1 = new javax.swing.JLabel();
        lblTel1 = new javax.swing.JLabel();
        lblPoint1 = new javax.swing.JLabel();
        lblTotal3 = new javax.swing.JLabel();
        lblDiscount1 = new javax.swing.JLabel();
        lblDiscount2 = new javax.swing.JLabel();
        lblSubtotal1 = new javax.swing.JLabel();
        lblSubtotal2 = new javax.swing.JLabel();
        lblCP = new javax.swing.JLabel();
        lblCP2 = new javax.swing.JLabel();
        lblC2 = new javax.swing.JLabel();
        lblC = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        lblDcoffee.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblDcoffee.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblDcoffee.setText("D-Coffee");

        lblLogo.setBackground(new java.awt.Color(255, 255, 255));
        lblLogo.setOpaque(true);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("อาคารวิทยาศาสตร์การแพทย์ คณะสหเวชศาสตร์");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("ม.บูรพา โทร 093-555-9999");

        lblDate.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lblDate.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblDate.setText("Date");

        lblTime.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lblTime.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblTime.setText("Time");

        lblUserName.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lblUserName.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblUserName.setText("UserName");

        tblReciept.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tblReciept.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblReciept);

        lblTotal2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblTotal2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblTotal2.setText("00.00");

        lblThankYou.setFont(new java.awt.Font("Segoe UI", 1, 16)); // NOI18N
        lblThankYou.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblThankYou.setText("Thank you. Have a nice day.");

        jLabel3.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("------------------------------------------------------");

        jLabel4.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("------------------------------------------------------");

        jLabel5.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("------------------------------------------------------");

        jLabel6.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("------------------------------------------------------");

        lblQueueNumber.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblQueueNumber.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblQueueNumber.setText("หมายเลขคิว");

        lblQueue.setFont(new java.awt.Font("Tahoma", 1, 30)); // NOI18N
        lblQueue.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblQueue.setText("000");

        jLabel7.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setText("------------------------------------------------------");

        lblMemberName.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lblMemberName.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblMemberName.setText("MemberName");

        lblTel.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lblTel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblTel.setText("Tel.");

        lblPoint.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lblPoint.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblPoint.setText("Point");

        lblDate1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lblDate1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDate1.setText("Date :");

        lblTime1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lblTime1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblTime1.setText("Time :");

        lblUserName1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lblUserName1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblUserName1.setText("UserName :");

        lblMemberName1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lblMemberName1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblMemberName1.setText("MemberName :");

        lblTel1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lblTel1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblTel1.setText("Tel :");

        lblPoint1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lblPoint1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblPoint1.setText("Point :");

        lblTotal3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblTotal3.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblTotal3.setText("Total : ");

        lblDiscount1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lblDiscount1.setText("Discount :");

        lblDiscount2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lblDiscount2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblDiscount2.setText("Discount ");

        lblSubtotal1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lblSubtotal1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblSubtotal1.setText("Subtotal :");

        lblSubtotal2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lblSubtotal2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblSubtotal2.setText("Subtotal ");

        lblCP.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblCP.setText("-");

        lblCP2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblCP2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblCP2.setText("Total C/P");

        lblC2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lblC2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblC2.setText("-");

        lblC.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lblC.setText("-");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(102, 102, 102)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lblDcoffee, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblQueue, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblQueueNumber, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
                    .addComponent(lblLogo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblDate1)
                            .addComponent(lblUserName1)
                            .addComponent(lblTime1, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(lblDate, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(lblTime, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(lblUserName, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 319, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 319, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap())))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblMemberName1)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(lblPoint1, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
                                .addComponent(lblTel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblTel, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblMemberName, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblPoint, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(11, 11, 11))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 319, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 319, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(lblDiscount1, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(218, 218, 218))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(lblCP, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(lblCP2, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(lblSubtotal1, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(174, 174, 174)
                                    .addComponent(lblSubtotal2, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(lblTotal3, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(230, 230, 230))
                                .addComponent(lblDiscount2, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(lblTotal2, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(lblC, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(168, 168, 168)
                                    .addComponent(lblC2, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGap(15, 15, 15)))
                    .addComponent(lblThankYou, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 319, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblQueue)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblQueueNumber)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblLogo, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblDcoffee)
                .addGap(1, 1, 1)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblDate)
                    .addComponent(lblDate1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTime)
                    .addComponent(lblTime1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblUserName)
                    .addComponent(lblUserName1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 9, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblMemberName)
                    .addComponent(lblMemberName1))
                .addGap(7, 7, 7)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTel)
                    .addComponent(lblTel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPoint)
                    .addComponent(lblPoint1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblSubtotal1)
                    .addComponent(lblSubtotal2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblDiscount1)
                    .addComponent(lblDiscount2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTotal3)
                    .addComponent(lblTotal2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblCP)
                    .addComponent(lblCP2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblC)
                    .addComponent(lblC2, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblThankYou, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblC;
    private javax.swing.JLabel lblC2;
    private javax.swing.JLabel lblCP;
    private javax.swing.JLabel lblCP2;
    private javax.swing.JLabel lblDate;
    private javax.swing.JLabel lblDate1;
    private javax.swing.JLabel lblDcoffee;
    private javax.swing.JLabel lblDiscount1;
    private javax.swing.JLabel lblDiscount2;
    private javax.swing.JLabel lblLogo;
    private javax.swing.JLabel lblMemberName;
    private javax.swing.JLabel lblMemberName1;
    private javax.swing.JLabel lblPoint;
    private javax.swing.JLabel lblPoint1;
    private javax.swing.JLabel lblQueue;
    private javax.swing.JLabel lblQueueNumber;
    private javax.swing.JLabel lblSubtotal1;
    private javax.swing.JLabel lblSubtotal2;
    private javax.swing.JLabel lblTel;
    private javax.swing.JLabel lblTel1;
    private javax.swing.JLabel lblThankYou;
    private javax.swing.JLabel lblTime;
    private javax.swing.JLabel lblTime1;
    private javax.swing.JLabel lblTotal2;
    private javax.swing.JLabel lblTotal3;
    private javax.swing.JLabel lblUserName;
    private javax.swing.JLabel lblUserName1;
    private javax.swing.JTable tblReciept;
    // End of variables declaration//GEN-END:variables
}
