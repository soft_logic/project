/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package ui;

import Dao.CustomerDao;
import Dao.ProductDao;
import Dao.PromotionDao;
import Dao.UserDao;
import Service.CustomerService;
import Service.PromotionService;
import component.BuyProductable;
import component.ProductListPanel;
import model.Product;
import model.Reciept;
import model.RecieptDetail;
import Service.RecieptService;
import Service.UserService;
import static Service.UserService.current;
import Service.ValidateException;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.chrono.Chronology;
import java.time.chrono.ThaiBuddhistChronology;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import model.Customer;

import model.Promotion;

import model.User;

/**
 *
 * @author User
 */
public class PosFrame extends javax.swing.JFrame implements BuyProductable {

    int count = 1;
    private Reciept reciept;
    private ProductDao productDao = new ProductDao();
    private Customer customer;
    private CustomerDao customerDao = new CustomerDao();
    RecieptService recieptService = new RecieptService();
    private PromotionService promotionService = new PromotionService();
    private PromotionDao promotionDao = new PromotionDao();
    private ArrayList<Promotion> promotions = (ArrayList<Promotion>) promotionDao.getAll();
    private Promotion promotion;
    private final ProductListPanel productListPanel;
    private User currentUser;
    private Object recieptDetails;
    private float total;
    private float discount=0;
    private float dismax = 0;
    private String promotionName = "";

    /**
     * Creates new form PosFrame
     */
    private UserDao user = new UserDao();
    
    public PosFrame(User User) throws ValidateException {
        initComponents();
        setStatus();
        currentUser = current;
        reciept = new Reciept();
        lblUserName.setText("User : " + currentUser.getName());
        lblStatus.setText("Status : " + currentUser.getRole(""));
        //reciept.setUser(UserService.getCurrent());
        Timer timer = new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                timerActionPerformed(evt);
            }
        });
        // เริ่มต้น Timer
        timer.start();
        

        

        ImageIcon icon = new ImageIcon("./Logo.png");
        Image image = icon.getImage();
        Image newImage = image.getScaledInstance(107, 100, Image.SCALE_SMOOTH);
        icon.setImage(newImage);
        pCoffee.setIcon(icon);

        icon = new ImageIcon("./User" + currentUser.getId() + ".png");
        image = icon.getImage();
        int width = image.getWidth(null);
        int height = image.getHeight(null);
        newImage = image.getScaledInstance(100, 100, Image.SCALE_SMOOTH);
        icon.setImage(newImage);
        pUser.setIcon(icon);

        tblReciepDetail.setModel(new AbstractTableModel() {
            String[] header = {"Name", "Price", "Qty", "Type", "Sweet", "TotalPrice"};

            @Override
            public String getColumnName(int column) {
                return header[column];
            }

            @Override
            public int getRowCount() {
                return reciept.getRecieptDetails().size();
            }

            @Override
            public int getColumnCount() {
                return 6;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ArrayList<RecieptDetail> recieptDetails = reciept.getRecieptDetails();
                RecieptDetail recieptDetail = recieptDetails.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return recieptDetail.getProductName();
                    case 1:
                        return recieptDetail.getProductPrice();
                    case 2:
                        return recieptDetail.getQty();
                    case 3:
                        return recieptDetail.getProductType();
                    case 4:
                        return recieptDetail.getProductSweet();
                    case 5:
                        return recieptDetail.getTotalPrice();
                    default:
                        throw new AssertionError();
                }

            }

            @Override
            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
                ArrayList<RecieptDetail> recieptDetails = reciept.getRecieptDetails();
                RecieptDetail recieptDetail = recieptDetails.get(rowIndex);
                if (columnIndex == 2) {
                    int qty = Integer.parseInt((String) aValue);
                    if (qty < 1) {
                        return;
                    }
                    recieptDetail.setQty(qty);
                    reciept.calculateTotal();
                    refreshReciept();
                }

            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                switch (columnIndex) {
                    case 2:
                        return true;

                    default:
                        return false;
                }
            }

        });

        txtCash.addKeyListener(new KeyAdapter() {
            public void keyReleased(KeyEvent e) {
                try {
                    int cash = Integer.parseInt(txtCash.getText());
                    reciept.setCash(cash);
                    float change = cash - (reciept.getTotal()-dismax);
                    lblChange.setText(change + "");
                } catch (NumberFormatException ex) {
                    lblChange.setText("");
                }
            }
        });
        
       
        
        productListPanel = new ProductListPanel();
        productListPanel.addOnBuyProduct(this);
        scrProductList.setViewportView(productListPanel);
    }
    
   
    

    private void timerActionPerformed(java.awt.event.ActionEvent evt) {

        LocalDateTime now = LocalDateTime.now();

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");

        lblTime.setText("" + now.format(formatter));
    }

    public void refreshReciept() {
        tblReciepDetail.revalidate();
        tblReciepDetail.repaint();
        setDis();
        lblSubtotalP.setText("" + reciept.getTotal() + "฿");
        lblTotal.setText("" + (reciept.getTotal() - dismax) + "฿");
        
        ArrayList<RecieptDetail> recieptDetails = reciept.getRecieptDetails();
        for (RecieptDetail r : recieptDetails) {
            System.out.println(r);
        }

    }
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        lblTime = new javax.swing.JLabel();
        lblUserName = new javax.swing.JLabel();
        lblStatus = new javax.swing.JLabel();
        pUser = new javax.swing.JLabel();
        pCoffee = new javax.swing.JLabel();
        lblHeader = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblReciepDetail = new javax.swing.JTable();
        btnDetele = new javax.swing.JButton();
        btnMainManu = new javax.swing.JButton();
        lblProductList = new javax.swing.JLabel();
        lblItemList = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        lblCash = new javax.swing.JLabel();
        txtCash = new javax.swing.JTextField();
        lblNChange = new javax.swing.JLabel();
        lblTotal = new javax.swing.JLabel();
        lblMamberDiscount = new javax.swing.JLabel();
        lblSubtotal = new javax.swing.JLabel();
        lblSubtotalP = new javax.swing.JLabel();
        lblMamberDiscountP = new javax.swing.JLabel();
        lbl฿ = new javax.swing.JLabel();
        lbl฿2 = new javax.swing.JLabel();
        lblMemberName1 = new javax.swing.JLabel();
        lblMemberName2 = new javax.swing.JLabel();
        lblTel = new javax.swing.JLabel();
        lblPoint = new javax.swing.JLabel();
        lblChange = new javax.swing.JLabel();
        lblTotal1 = new javax.swing.JLabel();
        lblPromotionName = new javax.swing.JLabel();
        lblPromotionName1 = new javax.swing.JLabel();
        scrProductList = new javax.swing.JScrollPane();
        jPanel4 = new javax.swing.JPanel();
        btnCalculate = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        btnRegisterMember = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        btnPromptPay = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(231, 208, 185));

        jPanel2.setBackground(new java.awt.Color(109, 89, 80));
        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2));

        lblTime.setFont(new java.awt.Font("EucrosiaUPC", 1, 36)); // NOI18N
        lblTime.setForeground(new java.awt.Color(255, 255, 255));
        lblTime.setText("Date");

        lblUserName.setFont(new java.awt.Font("EucrosiaUPC", 1, 36)); // NOI18N
        lblUserName.setForeground(new java.awt.Color(255, 255, 255));
        lblUserName.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblUserName.setText("User : ภัทรพล สุดประเสริฐ");

        lblStatus.setFont(new java.awt.Font("EucrosiaUPC", 1, 36)); // NOI18N
        lblStatus.setForeground(new java.awt.Color(255, 255, 255));
        lblStatus.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblStatus.setText("Status : Staff");

        pUser.setBackground(new java.awt.Color(102, 102, 102));

        pCoffee.setBackground(new java.awt.Color(96, 59, 24));

        lblHeader.setBackground(new java.awt.Color(255, 51, 51));
        lblHeader.setFont(new java.awt.Font("EucrosiaUPC", 1, 70)); // NOI18N
        lblHeader.setForeground(new java.awt.Color(255, 255, 255));
        lblHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblHeader.setText("Point of Sell");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(pCoffee, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(lblHeader, javax.swing.GroupLayout.DEFAULT_SIZE, 500, Short.MAX_VALUE)
                .addGap(351, 351, 351)
                .addComponent(pUser, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(lblUserName, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(lblTime, javax.swing.GroupLayout.PREFERRED_SIZE, 286, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(lblHeader, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(38, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pCoffee, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(pUser, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(lblTime)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(lblUserName)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(lblStatus)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        tblReciepDetail.setAutoCreateRowSorter(true);
        tblReciepDetail.setBackground(new java.awt.Color(214, 172, 150));
        tblReciepDetail.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tblReciepDetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblReciepDetail.setGridColor(new java.awt.Color(0, 0, 0));
        tblReciepDetail.setSelectionBackground(new java.awt.Color(255, 0, 0));
        tblReciepDetail.setShowGrid(true);
        jScrollPane2.setViewportView(tblReciepDetail);

        btnDetele.setBackground(new java.awt.Color(201, 136, 96));
        btnDetele.setFont(new java.awt.Font("EucrosiaUPC", 1, 24)); // NOI18N
        btnDetele.setText("Detele");
        btnDetele.setBorder(new javax.swing.border.MatteBorder(null));
        btnDetele.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeteleActionPerformed(evt);
            }
        });

        btnMainManu.setBackground(new java.awt.Color(201, 136, 96));
        btnMainManu.setFont(new java.awt.Font("EucrosiaUPC", 1, 24)); // NOI18N
        btnMainManu.setText("Main Manu");
        btnMainManu.setBorder(new javax.swing.border.MatteBorder(null));
        btnMainManu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMainManuActionPerformed(evt);
            }
        });

        lblProductList.setFont(new java.awt.Font("EucrosiaUPC", 1, 48)); // NOI18N
        lblProductList.setText("Product List");

        lblItemList.setFont(new java.awt.Font("EucrosiaUPC", 1, 48)); // NOI18N
        lblItemList.setText("Item List");

        jPanel3.setBackground(new java.awt.Color(231, 208, 185));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        lblCash.setFont(new java.awt.Font("EucrosiaUPC", 1, 24)); // NOI18N
        lblCash.setText("Cash :");

        txtCash.setFont(new java.awt.Font("EucrosiaUPC", 1, 24)); // NOI18N

        lblNChange.setFont(new java.awt.Font("EucrosiaUPC", 1, 24)); // NOI18N
        lblNChange.setText("Change :");

        lblTotal.setFont(new java.awt.Font("EucrosiaUPC", 1, 24)); // NOI18N
        lblTotal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblTotal.setText("00.00 ฿");
        lblTotal.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);

        lblMamberDiscount.setFont(new java.awt.Font("EucrosiaUPC", 1, 24)); // NOI18N
        lblMamberDiscount.setText("Member Discount :");

        lblSubtotal.setFont(new java.awt.Font("EucrosiaUPC", 1, 24)); // NOI18N
        lblSubtotal.setText("Subtotal :");

        lblSubtotalP.setFont(new java.awt.Font("EucrosiaUPC", 1, 24)); // NOI18N
        lblSubtotalP.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblSubtotalP.setText("00.00 ฿");

        lblMamberDiscountP.setFont(new java.awt.Font("EucrosiaUPC", 1, 24)); // NOI18N
        lblMamberDiscountP.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblMamberDiscountP.setText("0.00 ฿");

        lbl฿.setFont(new java.awt.Font("EucrosiaUPC", 1, 24)); // NOI18N
        lbl฿.setText("฿");

        lbl฿2.setFont(new java.awt.Font("EucrosiaUPC", 1, 24)); // NOI18N
        lbl฿2.setText("฿");

        lblMemberName1.setFont(new java.awt.Font("EucrosiaUPC", 1, 24)); // NOI18N
        lblMemberName1.setText("Member Name :");

        lblMemberName2.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N

        lblTel.setFont(new java.awt.Font("EucrosiaUPC", 1, 24)); // NOI18N
        lblTel.setText("Tel : ");

        lblPoint.setFont(new java.awt.Font("EucrosiaUPC", 1, 24)); // NOI18N
        lblPoint.setText("Point : ");

        lblChange.setBackground(new java.awt.Color(255, 255, 255));
        lblChange.setFont(new java.awt.Font("EucrosiaUPC", 1, 24)); // NOI18N
        lblChange.setAutoscrolls(true);
        lblChange.setOpaque(true);

        lblTotal1.setFont(new java.awt.Font("EucrosiaUPC", 1, 24)); // NOI18N
        lblTotal1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblTotal1.setText("Total :");
        lblTotal1.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);

        lblPromotionName.setFont(new java.awt.Font("EucrosiaUPC", 1, 24)); // NOI18N
        lblPromotionName.setText("Promotion Name :");

        lblPromotionName1.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(lblTel, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(31, 31, 31)
                        .addComponent(lblPoint, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(lblPromotionName)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblPromotionName1, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(lblMemberName1, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblMemberName2, javax.swing.GroupLayout.DEFAULT_SIZE, 186, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(lblTotal1, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(44, 44, 44)
                        .addComponent(lblTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(lblSubtotal, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(129, 129, 129)
                        .addComponent(lblSubtotalP, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(lblMamberDiscount)
                        .addGap(80, 80, 80)
                        .addComponent(lblMamberDiscountP, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(61, 61, 61)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblCash, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblNChange, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtCash, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblChange, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbl฿)
                    .addComponent(lbl฿2))
                .addGap(18, 18, 18))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblCash)
                            .addComponent(txtCash, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbl฿))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblNChange, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbl฿2)
                            .addComponent(lblChange, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblSubtotal)
                                .addComponent(lblSubtotalP))
                            .addComponent(lblPromotionName)
                            .addComponent(lblPromotionName1, javax.swing.GroupLayout.DEFAULT_SIZE, 23, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblMemberName2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(lblMamberDiscount)
                                        .addComponent(lblMamberDiscountP))
                                    .addComponent(lblMemberName1))
                                .addGap(0, 10, Short.MAX_VALUE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblPoint)
                            .addComponent(lblTotal1, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblTel))))
                .addContainerGap())
        );

        scrProductList.setBackground(new java.awt.Color(214, 172, 150));
        scrProductList.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jPanel4.setBackground(new java.awt.Color(231, 208, 185));

        btnCalculate.setBackground(new java.awt.Color(187, 237, 148));
        btnCalculate.setFont(new java.awt.Font("EucrosiaUPC", 1, 24)); // NOI18N
        btnCalculate.setText("Confirm");
        btnCalculate.setBorder(new javax.swing.border.MatteBorder(null));
        btnCalculate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCalculateActionPerformed(evt);
            }
        });

        btnCancel.setBackground(new java.awt.Color(255, 51, 51));
        btnCancel.setFont(new java.awt.Font("EucrosiaUPC", 1, 24)); // NOI18N
        btnCancel.setText("Cancel");
        btnCancel.setBorder(new javax.swing.border.MatteBorder(null));
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        btnRegisterMember.setBackground(new java.awt.Color(201, 136, 96));
        btnRegisterMember.setFont(new java.awt.Font("EucrosiaUPC", 1, 24)); // NOI18N
        btnRegisterMember.setText("Register Member");
        btnRegisterMember.setBorder(new javax.swing.border.MatteBorder(null));
        btnRegisterMember.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegisterMemberActionPerformed(evt);
            }
        });

        jButton2.setBackground(new java.awt.Color(201, 136, 96));
        jButton2.setFont(new java.awt.Font("EucrosiaUPC", 1, 24)); // NOI18N
        jButton2.setText("Member Search");
        jButton2.setBorder(new javax.swing.border.MatteBorder(null));
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        btnPromptPay.setBackground(new java.awt.Color(201, 136, 96));
        btnPromptPay.setFont(new java.awt.Font("EucrosiaUPC", 1, 24)); // NOI18N
        btnPromptPay.setText("PromptPay");
        btnPromptPay.setBorder(new javax.swing.border.MatteBorder(null));
        btnPromptPay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPromptPayActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnPromptPay, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnRegisterMember, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnCalculate, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(28, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCalculate, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRegisterMember, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPromptPay, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addComponent(btnMainManu, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblProductList, javax.swing.GroupLayout.PREFERRED_SIZE, 226, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(scrProductList, javax.swing.GroupLayout.PREFERRED_SIZE, 501, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jScrollPane2)
                                .addGap(5, 5, 5))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addContainerGap())
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblItemList, javax.swing.GroupLayout.PREFERRED_SIZE, 226, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btnDetele, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, Short.MAX_VALUE))))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnMainManu, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblProductList, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblItemList, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 394, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnDetele, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(scrProductList, javax.swing.GroupLayout.PREFERRED_SIZE, 655, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(26, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCalculateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCalculateActionPerformed
        System.out.println("" + reciept);
        if(customer!=null){
            CustomerService customerService = new CustomerService();
        customer.setPoint((int) (customer.getPoint()+((reciept.getTotal()-dismax)/10)));
        customerService.update(customer);
        }
        recieptService.addNew(reciept);
        ResetText();
        countFormatted(); 
        
        RecieptDialog();
        clearReciept();
        
    }//GEN-LAST:event_btnCalculateActionPerformed
       public void ResetText() {
        txtCash.setText("");
        lblChange.setText("");
    }
    private void btnMainManuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMainManuActionPerformed
       MainFrame mainFrame = new MainFrame(current);
        mainFrame.setVisible(true);  // เปิดหน้าต่าง MainFrame
        dispose();
    }//GEN-LAST:event_btnMainManuActionPerformed


    private void btnDeteleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeteleActionPerformed
        int row = tblReciepDetail.getSelectedRow();
        if (row >= 0) {
            dismax = 0;
            reciept.getRecieptDetails().remove(row);
            promotion = null;
            setTextDisName(promotion);
            setTextDis();
            recalculateTotal(reciept);
            refreshReciept();
        }
    }//GEN-LAST:event_btnDeteleActionPerformed

    private void btnPromptPayActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPromptPayActionPerformed
        PromptPayDialog promptPayDialog = new PromptPayDialog(this, true, reciept.getTotal(), reciept, recieptService, count,dismax, customer);
        promptPayDialog.setLocationRelativeTo(this);
        promptPayDialog.setVisible(true);
        
        promptPayDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                if (promptPayDialog.getRefresh() == 1) {
                    clearReciept();
                }
            }
        });
    }//GEN-LAST:event_btnPromptPayActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        clearReciept();
    }//GEN-LAST:event_btnCancelActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        openDialogCustomer();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void btnRegisterMemberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegisterMemberActionPerformed
        openDialogRegister();
    }//GEN-LAST:event_btnRegisterMemberActionPerformed
       
     public String countFormatted() {
        String countFormatted = String.format("%03d", count);
        return countFormatted;
    }
    
    private void recalculateTotal(Reciept reciept) {
        total = 0;
        for (RecieptDetail detail : reciept.getRecieptDetails()) {
            total += detail.getTotalPrice();
        }
        reciept.setTotal(total);
    }

    public void clearReciept() {
        customer = null;
        promotion = null;
        dismax = 0;
        setNameCustomer();
        setTextDis();
        reciept = new Reciept();
        reciept.setUser(currentUser);
        //reciept.setUser(UserService.getCurrent());
        refreshReciept();
    }
    //-----------------------------------------------Promotion------------------------------------------------------------
     private void setTextDis() {
         if(promotion ==null){
         lblPromotionName1.setText("");
            lblMamberDiscountP.setText("0.0฿");
         }else{
            if (discount > dismax) {
               dismax = discount;
               lblPromotionName1.setText(promotion.getName()); 
               promotionName = promotion.getName();
               reciept.setPromotion_name(promotionName);
//               reciept.setPromotion_name(promotion.getName());
               lblMamberDiscountP.setText(""+dismax);
               
           }
            
         }
        
        
    }
     private void checkPoint() {
        if (customer != null) {
            if (customer.getPoint() / 10 >= 1) {
                int choice = JOptionPane.showConfirmDialog(null, "ตอนนี้คุณมี " + customer.getPoint() + " แต้มพอที่จะใช้เป็นส่วนลดหรือไม่?", "ตรวจสอบแต้ม", JOptionPane.YES_NO_OPTION);

                if (choice == JOptionPane.YES_OPTION) {
                    dismax += customer.getPoint() / 10;
                    lblMamberDiscountP.setText(dismax + " ฿");
//                    lblTotal.setText("" + (reciept.getTotal() - dismax) + "฿");
                    customer.setPoint((int) (customer.getPoint() % 10 + (total / 10)));
                    customerDao.update(customer);
                    setNameCustomer();
                }

            }
            refreshReciept();
        }
    }
     
     
     private void openDialogCustomer() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        CustomerbyTel CustomerByTel = new CustomerbyTel(frame,customer);
        CustomerByTel.setLocationRelativeTo(this);
        CustomerByTel.setVisible(true);
        
        CustomerByTel.addWindowListener(new WindowAdapter(){
            @Override
            public void windowClosed(WindowEvent e) {
              customer = CustomerByTel.getCustomer();
              setNameCustomer();
              checkPoint();
            }
            
        });
    }
     private void setNameCustomer(){
         if(customer ==null){
         lblMemberName2.setText("");
        lblPoint.setText("Point : ");
        lblTel.setText("Tel : ");
         }else{ lblMemberName2.setText(customer.getName());
        lblPoint.setText("Point : "+customer.getPoint());
        lblTel.setText("Tel : "+customer.getTel());
         }
       
    }
     public void setDis() {
         checkUnit();
        disProduct();
        disAll();
        
    }

//    private void discountUnit() {
//        ArrayList<RecieptDetail> recieptDetails = reciept.getRecieptDetails();
//        int unit = checkUnit();
//        if (unit != 0) {
//            discount = 0;
//            //เรียงลำดับ recieptDetails ตามราคามากไปน้อย
//            Collections.sort(recieptDetails, new Comparator<RecieptDetail>() {
//                @Override
//                public int compare(RecieptDetail r1, RecieptDetail r2) {
//                    return Float.compare(r2.getProductPrice(), r1.getProductPrice());
//                }
//            });
//            
//                
//                    for (RecieptDetail r : recieptDetails) {
//                    total = reciept.getTotal();
//                    discount = (float) (Math.floor(total * p.getDiscount()));
//                    setTextDis();
//
//            }
//
//        }
//    }

    private void checkUnit() {
        ArrayList<RecieptDetail> recieptDetails = reciept.getRecieptDetails();
        Collections.sort(promotions, new Comparator<Promotion>() {
                    @Override
                    public int compare(Promotion r1, Promotion r2) {
                        return Integer.compare(r2.getUnit(), r1.getUnit());
                    }
                });
        for (Promotion p : promotions) {
            if(checkStatus(p)){
            if (p.getUnit() != 0) {
                int count = 0;
                for (RecieptDetail r : recieptDetails) {
                    count += r.getQty();
                    System.out.println(r);
                }
                if (count >= p.getUnit()) {
                    total = reciept.getTotal();
                    discount = (float) (Math.floor(total * p.getDiscount()));
                    setTextDisName(p);
                    setTextDis();
                    
                    
                }
            }
            }

        }
        
    }

    private void disProduct() {
        ArrayList<RecieptDetail> recieptDetails = reciept.getRecieptDetails();
        for (Promotion p : promotions) {
            if(checkStatus(p)){
            
            discount = 0;
            if(!p.getProduct().equals("")){
                String[] nameProduct = p.getProduct().split(",");           
                if(nameProduct.length>1){
                    checkDisSet(nameProduct, recieptDetails, p);
                }else{
                    for (RecieptDetail r : recieptDetails) {
                        if (r.getProductName().equals(p.getProduct())) {
                            discount += (float) (r.getProductPrice() * p.getDiscount() * r.getQty());
                        }
                    }
                } 
            }setTextDisName(p);
            setTextDis();
            }
        }
    }
    private void  checkDisSet(String[] nameProduct, ArrayList<RecieptDetail> recieptDetails, Promotion p) { 
        int count = 0;
        int disproduct =0;
        for (String name : nameProduct) {
            for (RecieptDetail r : recieptDetails) {
                if (r.getProductName().equals(name)) {
                    disproduct += (float) (r.getProductPrice()*p.getDiscount());
                    count+=1;
                    break;
                }
            }
        }
        if(count ==nameProduct.length){
            discount = disproduct;
            setTextDisName(p);
        setTextDis();
        }
      
    }
    private void setTextDisName(Promotion p) {
        promotion = p;
    }

    private void disAll() {
        for (Promotion p : promotions) {
            if(checkStatus(p)){
            
            if (p.getUnit() == 0) {
                if (p.getProduct().equals("")) {
                    total = reciept.getTotal();
                    discount = (float) (Math.floor(total * p.getDiscount()));
                    setTextDisName(p);
                    setTextDis();
                    
                }
            }
        }
        }
    }
    private void setStatus() throws ValidateException{
    for(Promotion p :promotions){
        LocalDate startDate = LocalDate.parse(p.getStart());
        LocalDate endDate = LocalDate.parse(p.getEnd());
        LocalDate currentDate = LocalDate.now(); // วันปัจจุบัน
       int buddhistYear = currentDate.getYear() + 543; // ค.ศ. + 543 = พ.ศ.

        // สร้าง LocalDate ในระบบพ.ศ. โดยใช้ปีในระบบค.ศ.
        currentDate = LocalDate.of(buddhistYear, currentDate.getMonthValue(), currentDate.getDayOfMonth());

        try{
        if (currentDate.equals(startDate) || currentDate.equals(endDate) || 
    (currentDate.isAfter(startDate) && currentDate.isBefore(endDate))) {
            p.setStatus(1);
            
        
        
       dispose();
            promotionService.update(p);
        } else {
            p.setStatus(0);
            promotionService.update(p);
        }} catch (ValidateException ex) {
                Logger.getLogger(PosFrame.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(this, ex.getMessage());
                return ;
        }
    }
    }
    private boolean checkStatus(Promotion p){
        return p.getStatus()==1;
    }
    private void openDialogRegister() {
        customer = new Customer();
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        CustomerDialog customerDialog = new CustomerDialog(frame,customer);
        customerDialog.setLocationRelativeTo(this);
        customerDialog.setVisible(true);
        customerDialog.addWindowListener(new WindowAdapter(){
            @Override
            public void windowClosed(WindowEvent e) {
                if(customerDialog.getCustoemr().getId()!=-1){
                    customer = customerDialog.getCustoemr();
                    setNameCustomer();
                }else{
                customer = null;
                }
            }
            
        });
    }
    //---------------------------------------------end promotion----------------------------------------------------------

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PosFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PosFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PosFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PosFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                User i = new User();
                i = UserService.current;
                try {
                    new PosFrame(i).setVisible(true);
                } catch (ValidateException ex) {
                    Logger.getLogger(PosFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCalculate;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnDetele;
    private javax.swing.JButton btnMainManu;
    private javax.swing.JButton btnPromptPay;
    private javax.swing.JButton btnRegisterMember;
    private javax.swing.JButton jButton2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblCash;
    private javax.swing.JLabel lblChange;
    private javax.swing.JLabel lblHeader;
    private javax.swing.JLabel lblItemList;
    private javax.swing.JLabel lblMamberDiscount;
    private javax.swing.JLabel lblMamberDiscountP;
    private javax.swing.JLabel lblMemberName1;
    private javax.swing.JLabel lblMemberName2;
    private javax.swing.JLabel lblNChange;
    private javax.swing.JLabel lblPoint;
    private javax.swing.JLabel lblProductList;
    private javax.swing.JLabel lblPromotionName;
    private javax.swing.JLabel lblPromotionName1;
    private javax.swing.JLabel lblStatus;
    private javax.swing.JLabel lblSubtotal;
    private javax.swing.JLabel lblSubtotalP;
    private javax.swing.JLabel lblTel;
    private javax.swing.JLabel lblTime;
    private javax.swing.JLabel lblTotal;
    private javax.swing.JLabel lblTotal1;
    private javax.swing.JLabel lblUserName;
    private javax.swing.JLabel lbl฿;
    private javax.swing.JLabel lbl฿2;
    private javax.swing.JLabel pCoffee;
    private javax.swing.JLabel pUser;
    private javax.swing.JScrollPane scrProductList;
    private javax.swing.JTable tblReciepDetail;
    private javax.swing.JTextField txtCash;
    // End of variables declaration//GEN-END:variables
    public void buy(Product product, int qty) {
        reciept.addRecieptDetail(product, qty);
        refreshReciept();
    }

    private void RecieptDialog() {
         RecieptDialog recieptDialog = new RecieptDialog(current, countFormatted(),reciept.getTotal(), reciept, recieptService,dismax, customer);
        count++;
        recieptDialog.setCash();
        recieptDialog.setLocationRelativeTo(this);
        recieptDialog.setVisible(true);
        recieptDialog.addWindowListener(new WindowAdapter() {

        });
    }

    @Override
    public void buy(Product product, int qty, String type, String sweet) {
        reciept.addRecieptDetail(product, qty, type, sweet);
        refreshReciept();
    }

}
