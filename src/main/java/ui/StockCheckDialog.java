/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package ui;

import Service.CheckStoreService;
import Service.MaterialService;
import Service.UserService;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import model.CheckStore;
import model.CheckStoreDetail;
import model.Material;
/**
 *
 * @author kantinan
 */
public class StockCheckDialog extends javax.swing.JDialog {

    private final CheckStoreService checkStoreService;
    private CheckStore editedCheckStore;
    CheckStore checkStore;
    private List<CheckStore> checkStoreList;
    CheckStoreDetail checkStoreDetail;
    private final MaterialService mtrService;
    private List<Material> mtrList;

    /**
     * Creates new form StockCheckDialog
     */
    public StockCheckDialog(java.awt.Frame parent, CheckStore editedCheckStore) {
        super(parent, true);
        initComponents();
     
        checkStore = new CheckStore();
        checkStore.setUserId(UserService.getCurrent().getId());
        this.editedCheckStore = editedCheckStore;

        checkStoreService = new CheckStoreService();
        checkStoreList = checkStoreService.getCheckStores();
        mtrService = new MaterialService();
        mtrList = mtrService.getMaterials();
        mtrList = mtrService.getMaterialsOrderByName();

        tblMat.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 14));
        tblMat.setModel(new AbstractTableModel() {
            String[] columnNames = {"ID", "NAME",
                "MINIMUM", "QTY", "UNIT"};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return mtrList.size();
            }

            @Override
            public int getColumnCount() {
                return 4;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Material mtr = mtrList.get(rowIndex);
                switch (columnIndex) {
                   
                    case 0:
                        return mtr.getMaterialID();
                    case 1:
                        return mtr.getMaterialName();
                    case 2:
                        return mtr.getMaterialMin();
                    case 3:
                        return mtr.getMaterialQty();

                    default:
                        return "Unknowm";
                }
            }
        });

        tblCheck.setModel(new AbstractTableModel() {
            String[] headers = {"Name", "Qty"};

            @Override
            public String getColumnName(int column) {
                return headers[column];
            }

            @Override
            public int getRowCount() {
                return checkStore.getCheckStoreDetail().size();
            }

            @Override
            public int getColumnCount() {
                return 2;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ArrayList<CheckStoreDetail> checkStoreDetails = checkStore.getCheckStoreDetail();
                CheckStoreDetail checkStoreDetail = checkStoreDetails.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return checkStoreDetail.getMaterialName();
                    case 1:
                        return checkStoreDetail.getMaterialQty();
                    default:
                        return "";
                }
            }

            @Override
            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
                ArrayList<CheckStoreDetail> checkStoreDetails = checkStore.getCheckStoreDetail();
                CheckStoreDetail checkStoreDetail = checkStoreDetails.get(rowIndex);
                if (columnIndex == 1) {
                    int qty = Integer.parseInt((String) aValue);
                    if (qty < 1) {
                        return;
                    }
                    checkStoreDetail.setMaterialQty(qty);
                }
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                switch (columnIndex) {
                    case 1:

                        return true;
                    default:
                        return false;
                }
            }

        });

        tblMat.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = tblMat.rowAtPoint(e.getPoint());
                int col = tblMat.columnAtPoint(e.getPoint());
                Material material = mtrList.get(row);
                

                for (CheckStoreDetail detail : checkStore.getCheckStoreDetail()) {
                    if (detail.getMaterialId() == material.getMaterialID()) {
                        return;
                    }
                }
                checkStore.addCheckStoreDetail(material, material.getMaterialQty());
                tblCheck.revalidate();
                tblCheck.repaint();
            }
        });
    }

//    private void refreshRecipt() {
//        tblCheck.revalidate();
//        tblCheck.repaint();
//        lblTotal.setText("Total: " + checkStore.get());
//    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblCheck = new javax.swing.JTable();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblMat = new javax.swing.JTable();
        btnCheck = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(241, 226, 210));

        tblCheck.setBackground(new java.awt.Color(214, 172, 150));
        tblCheck.setFont(new java.awt.Font("EucrosiaUPC", 0, 18)); // NOI18N
        tblCheck.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblCheck.setRowHeight(40);
        tblCheck.setShowGrid(true);
        jScrollPane2.setViewportView(tblCheck);

        tblMat.setBackground(new java.awt.Color(214, 172, 150));
        tblMat.setFont(new java.awt.Font("EucrosiaUPC", 0, 18)); // NOI18N
        tblMat.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblMat.setRowHeight(40);
        tblMat.setShowGrid(true);
        jScrollPane1.setViewportView(tblMat);

        btnCheck.setBackground(new java.awt.Color(201, 136, 96));
        btnCheck.setFont(new java.awt.Font("EucrosiaUPC", 1, 24)); // NOI18N
        btnCheck.setText("Check");
        btnCheck.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCheckActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("EucrosiaUPC", 1, 48)); // NOI18N
        jLabel1.setText("CheckStock");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(41, 41, 41)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnCheck))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 470, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 46, Short.MAX_VALUE)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 470, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(40, 40, 40))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCheck, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 503, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 503, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(66, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCheckActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCheckActionPerformed
        System.out.println("" + checkStore);
        checkStoreService.addNew(checkStore);
        this.dispose();
    }//GEN-LAST:event_btnCheckActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCheck;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tblCheck;
    private javax.swing.JTable tblMat;
    // End of variables declaration//GEN-END:variables

}
