/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Service;


import Dao.UserDao;
import model.User;
import java.util.List;
import ui.CheckInOutPanel;
/**
 *
 * @author werapan
 */
public class UserService {
    public static User current;
   public User login(String login, String password) {
        UserDao userDao = new UserDao();
        User user = userDao.getByLogin(login);
        if (user != null && user.getPassword().equals(password)) {
            current = user;  // กำหนดค่า current เป็นผู้ใช้ที่ล็อกอินสำเร็จ
            return user;
        }
        return null;  // การล็อกอินไม่สำเร็จ
    }


    public static User getCurrent() {
        return current;
    }
    
    public List<User> getUsers(){
        UserDao userDao = new UserDao();
        return userDao.getAll(" user_login asc");
    }

    public User addNew(User editedUser) {
        UserDao userDao = new UserDao();
        return userDao.save(editedUser);
    }

    public User update(User editedUser) {
        UserDao userDao = new UserDao();
        return userDao.update(editedUser);
    }

    public int delete(User editedUser) {
        UserDao userDao = new UserDao();
        return userDao.delete(editedUser);
    }

    public static void setCurrent(User current) {
        UserService.current = current;
    }
    public  String getName(int id) {
        UserDao userDao = new UserDao();
        User user = userDao.get(id);
        return user.getName();
    }
     public String getNameFromUniqueUserIds(int userId) {
    UserDao userDao = new UserDao();
    User user = userDao.get(userId);
    return user.getName();
}

   public List<User> getByName(String name){
        UserDao userDao = new UserDao();
        return userDao.getByName(name);
    }

    
     
}

