/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Service;

import Dao.BillCostDao;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import model.BillCost;

/**
 *
 * @author meepo
 */
public class BillCostService {

    public static String formatedDate(Date date) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String formatedDate = df.format(date);
        return formatedDate;
    }

    public List<BillCost> getBillCosts() {
        BillCostDao billCostDao = new BillCostDao();
        return billCostDao.getAll(" bill_id asc");
    }

    public BillCost addNew(BillCost editedbill) {
        BillCostDao billCostDao = new BillCostDao();
        Date period = new Date();
        editedbill.setBillDate(period);
        return billCostDao.save(editedbill);
    }

    public BillCost update(BillCost editedbill) {
        BillCostDao billCostDao = new BillCostDao();
        return billCostDao.update(editedbill);
    }

    public int delete(BillCost editedbill) {
        BillCostDao billCostDao = new BillCostDao();
        return billCostDao.delete(editedbill);
    }

    public BillCost getBillID(int id) {
        BillCostDao billCostDao = new BillCostDao();
        return billCostDao.get(id);
    }
}
