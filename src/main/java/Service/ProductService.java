/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Service;

import Dao.ProductDao;
import model.Product;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pattarapon
 */
public class ProductService {
    private final ProductDao productDao = new ProductDao();
    public ArrayList<Product> getProductsOrderByName(){
        return (ArrayList<Product>) productDao.getAll("product_name ASC");
    }
    public Product getById(int id) {
        ProductDao productdao = new ProductDao();
        Product product = productdao.get(id);
        return product;
    }
    public List<Product> getProduct(){
        ProductDao productdao = new ProductDao();
        return productdao.getAll(" product_id asc");
    }
    public List<Product> getByName(String name){
        ProductDao productdao = new ProductDao();
        return productdao.getByName(name);
    }

    public Product addNew(Product editedProduct) throws ValidateException {
        if(!editedProduct.isValid()){
            throw new ValidateException("Product Price invaid!!");
        }
        ProductDao productdao = new ProductDao();
        return productdao.save(editedProduct);
    }

    public Product update(Product editedProduct) throws ValidateException {
        if(!editedProduct.isValid()){
            throw new ValidateException("Product Price invaid!!");
        }
        ProductDao productdao = new ProductDao();
        return productdao.update(editedProduct);
    }

    public int delete(Product editedProduct) {
        ProductDao productdao = new ProductDao();
        return productdao.delete(editedProduct);
    }
}


