/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Service;


import Dao.PromotionDao;


import model.Promotion;

import java.util.List;

/**
 *
 * @author werapan
 */
public class PromotionService {
    
    
    public Promotion getById(int id){
           PromotionDao PromotionDao = new PromotionDao();
            return PromotionDao.get(id);
       }
    public List<Promotion> getPromotions(){
        PromotionDao PromotionDao = new PromotionDao();
        return PromotionDao.getAll(" promotion_id asc");
    }

    public List<Promotion> getByName(String name){
        PromotionDao promotionDao = new PromotionDao();
        return promotionDao.getByName(name);
    }
    public Promotion addNew(Promotion editedPromotion) throws ValidateException {
        if(!editedPromotion.isValid()){
            throw new ValidateException("Promotion discount invaid!!");
        }
        PromotionDao promotionDao = new PromotionDao();
        Promotion promotion = promotionDao.save(editedPromotion);
        return promotion;
    }

    public Promotion update(Promotion editedPromotion) throws ValidateException {
        if(!editedPromotion.isValid()){
            throw new ValidateException("Promotion discount invaid!!");
        }
        PromotionDao promotionDao = new PromotionDao();
        return promotionDao.update(editedPromotion);
    }

    public int delete(Promotion editedPromotion) {
        PromotionDao promotionDao = new PromotionDao();
        return promotionDao.delete(editedPromotion);
    }
}
