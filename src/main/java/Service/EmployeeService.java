/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Service;


import Dao.EmployeeDao;
import java.util.List;
import model.Employee;


/**
 *
 * @author Tncpop
 */
public class EmployeeService {
      public Employee getById(int id) {
        EmployeeDao employeedao = new EmployeeDao();
        Employee employee = employeedao.get(id);
        return employee;
    }
    public List<Employee> getEmployee(){
        EmployeeDao employeedao = new EmployeeDao();
        return employeedao.getAll(" emp_id asc");
    }

    public Employee addNew(Employee editedEmp) {
        EmployeeDao employeedao = new EmployeeDao();
        return employeedao.save(editedEmp);
    }

    public Employee update(Employee editedEmp) {
        EmployeeDao employeedao = new EmployeeDao();
        return employeedao.update(editedEmp);
    }

    public int delete(Employee editedEmp) {
        EmployeeDao employeedao = new EmployeeDao();
        return employeedao.delete(editedEmp);
    }
    public  String getName(int id) {
        EmployeeDao employeeDao = new EmployeeDao();
        Employee employee = employeeDao.get(id);

        return employee.getName();
    }
}
