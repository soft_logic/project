/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Service;

import Dao.SalaryDao;
import java.util.Date;
import java.util.List;
import model.Salary;

/**
 *
 * @author Acer
 */
public class SalaryService {
       public static Salary paySalary(Date date, double salary, int id) {
        SalaryDao payrollDao = new SalaryDao();
        Salary pay = new Salary(date, salary, id);
        return payrollDao.save(pay);
    }
    public List<Salary> getSalarys() {
        SalaryDao payrollDao = new SalaryDao();
        return payrollDao.getAll();
    }
}
