/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Service;

import Dao.Check_TimeDao;
import helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import model.Check_Time;
import model.User;

/**
 *
 * @author Acer
 */
public class Check_TimeService {

    public static List<Check_Time> getCheck_Time() {
        Check_TimeDao checkTimeDao = new Check_TimeDao();
        return checkTimeDao.getAll();
    }

    public static String formatedDate(Date date) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String formatedDate = df.format(date);
        return formatedDate;
    }

    public static void login(User user) throws ParseException {
        Check_TimeDao checkTimeDao = new Check_TimeDao();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date = new Date();
        Date dateOut = df.parse(df.format(date));
        Check_Time checkTime = new Check_Time(date, dateOut, 0, user.getId(),"n");
        checkTimeDao.save(checkTime);
    }

    public List<Check_Time> getCheckTime() {
        Check_TimeDao checkTimeDao = new Check_TimeDao();
        return checkTimeDao.getAll();
    }

    public List<Check_Time> getCheckTimeGroupByUserId() {
        List<Check_Time> checkTimes = new ArrayList<>();
        String sql = "SELECT user_id, MIN(check_in) AS check_in, MAX(check_out) AS check_out, SUM(check_workHour) AS total_workHour "
                + "FROM check_time "
                + "GROUP BY user_id";

        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Check_Time checkTime = new Check_Time();
                checkTime.setUserId(rs.getInt("user_id"));
                checkTime.setHour(rs.getInt("total_workHour"));
                checkTimes.add(checkTime);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return checkTimes;
    }
        public static void changePayStatus(Check_Time checktime) {
        Check_TimeDao checkTimeDao = new Check_TimeDao();
        checktime.setStatus("y");
        checkTimeDao.update(checktime);
    }

}


