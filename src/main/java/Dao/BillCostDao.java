/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Dao;

import helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import model.BillCost;
import model.Material;

/**
 *
 * @author meepo
 */
public class BillCostDao implements Dao<BillCost>{

    @Override
    public BillCost get(int id) {
         BillCost billcost = null;
        String sql = "SELECT * FROM bill_cost WHERE bill_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                billcost = BillCost.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return billcost;
    }

    @Override
    public List<BillCost> getAll() {
        ArrayList<BillCost> list = new ArrayList();
        String sql = "SELECT * FROM bill_cost";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                BillCost billcost = BillCost.fromRS(rs);
                list.add(billcost);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
     public List<BillCost> getAll(String order) {
        ArrayList<BillCost> list = new ArrayList();
        String sql = "SELECT * FROM bill_cost  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                BillCost billcost = BillCost.fromRS(rs);
                list.add(billcost);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public BillCost save(BillCost obj) {
      String sql = "INSERT INTO bill_cost(bill_typeid,bill_date,bill_cost)"
                + "VALUES(?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            stmt.setInt(1, obj.getBillTypeID());
            stmt.setString(2, df.format(obj.getBillDate()));
            stmt.setDouble(3, obj.getBillCost());
            
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setBillID(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }
    

    @Override
    public BillCost update(BillCost obj) {
String sql = "UPDATE bill_cost"
                + " SET bill_typeid = ?, bill_date = ?, bill_cost = ? "
                + " bill_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            stmt.setInt(1, obj.getBillTypeID());
            stmt.setString(2, df.format(obj.getBillDate()));
            stmt.setDouble(3, obj.getBillCost());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }
    

    @Override
    public int delete(BillCost obj) {
         String sql = "DELETE FROM bill_cost WHERE bill_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getBillID());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
    

    @Override
    public List<BillCost> getAll(String where, String order) {
       ArrayList<BillCost> list = new ArrayList();
        String sql = "SELECT * FROM bill_cost where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                 BillCost billcost = BillCost.fromRS(rs);
                list.add(billcost);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
