/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Dao;

import helper.DatabaseHelper;
import model.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.ProductReport;

/**
 *
 * @author werapan
 */
public class ProductDao implements Dao<Product> {

    @Override
    public Product get(int id) {
        Product product = null;
        String sql = "SELECT * FROM product WHERE product_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                product = Product.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return product;
    }

    public List<Product> getAll() {
        ArrayList<Product> list = new ArrayList();
        String sql = "SELECT * FROM product";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Product product = Product.fromRS(rs);
                list.add(product);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    public List<Product> getByName(String name) {
        ArrayList<Product> list = new ArrayList();
        String sql = "SELECT * FROM product WHERE product_name =?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Product product = Product.fromRS(rs);
                list.add(product);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
     public List<ProductReport> getTopseller(int limit) {
        ArrayList<ProductReport> list = new ArrayList();
        String sql = """
                     select pro.product_name,pro.product_price,
                     sum(recde.qty) as totalQty,sum(recde.qty*recde.product_price) totalPrice from product pro
                     inner join reciept_detail recde on recde.product_id=pro.product_id
                     inner join reciept rec on rec.reciept_id=recde.reciept_id
                     
                     group by pro.product_id
                     order by totalPrice desc
                     limit ?;""";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, limit);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ProductReport product = ProductReport.fromRS(rs);
                list.add(product);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    public List<ProductReport> getnonseller(int limit) {
        ArrayList<ProductReport> list = new ArrayList();
        String sql = """
                     select pro.product_name,pro.product_price,
                     sum(recde.qty) as totalQty,sum(recde.qty*recde.product_price) totalPrice from product pro
                     inner join reciept_detail recde on recde.product_id=pro.product_id
                     inner join reciept rec on rec.reciept_id=recde.reciept_id
                     
                     group by pro.product_id
                     order by totalPrice asc
                     limit ?;""";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, limit);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ProductReport product = ProductReport.fromRS(rs);
                list.add(product);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    public List<ProductReport> getTop(int limit,String begin,String end) {
        ArrayList<ProductReport> list = new ArrayList();
        String sql = """
                     select pro.product_name,pro.product_price,
                     sum(recde.qty) as totalQty,sum(recde.qty*recde.product_price) totalPrice from product pro
                     inner join reciept_detail recde on recde.product_id=pro.product_id
                     inner join reciept rec on rec.reciept_id=recde.reciept_id
                     where rec.created_date between ? and ?
                     
                     group by pro.product_id
                     order by totalPrice desc
                     limit ?;""";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(3, limit);
            stmt.setString(1, begin);
            stmt.setString(2, end);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ProductReport product = ProductReport.fromRS(rs);
                list.add(product);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    @Override
    public List<Product> getAll(String where, String order) {
        ArrayList<Product> list = new ArrayList();
        String sql = "SELECT * FROM product where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Product product = Product.fromRS(rs);
                list.add(product);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    

    public List<Product> getAll(String order) {
        ArrayList<Product> list = new ArrayList();
        String sql = "SELECT * FROM product  ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Product product = Product.fromRS(rs);
                list.add(product);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Product save(Product obj) {

        String sql = "INSERT INTO product (product_name, product_price , product_size,product_sweet_level , product_type, category_id)"
                + "VALUES(?, ?,?,?,?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setFloat(2, obj.getPrice());
            stmt.setString(3, obj.getSize());
            stmt.setString(4, obj.getSweetLevel());
            stmt.setString(5, obj.getType());
            stmt.setInt(6, obj.getCategoryId());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Product update(Product obj) {
        String sql = "UPDATE product"
                + " SET product_name =?, product_price =?, product_size=?,product_sweet_level =?, product_type=?, category_id=?"
                + " WHERE product_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setFloat(2, obj.getPrice());
            stmt.setString(3, obj.getSize());
            stmt.setString(4, obj.getSweetLevel());
            stmt.setString(5, obj.getType());
            stmt.setInt(6, obj.getCategoryId());
            stmt.setInt(7, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Product obj) {
        String sql = "DELETE FROM product WHERE product_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }

}
