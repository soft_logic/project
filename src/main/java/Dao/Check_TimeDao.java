/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Dao;

import helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import model.Check_Time;

/**
 *
 * @author Acer
 */
public class Check_TimeDao implements Dao<Check_Time>{

    @Override
    public Check_Time get(int id) {
        Check_Time check_time = null;
        String sql = "SELECT * FROM check_time WHERE check_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                check_time = check_time.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return check_time;
    }

    @Override
    public List<Check_Time> getAll() {
       ArrayList<Check_Time> list = new ArrayList();
        String sql = "SELECT * FROM check_time ORDER BY check_id DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Check_Time check_time = Check_Time.fromRS(rs);
                list.add(check_time);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Check_Time save(Check_Time obj) {
        String sql = "INSERT INTO check_time (check_in,check_out,check_workHour,user_id,check_pay)" + "VALUES(?, ?, ?, ?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            stmt.setString(1, df.format(obj.getIn()));
            stmt.setString(2, df.format(obj.getOut()));
            stmt.setInt(3, obj.getHour());
            stmt.setInt(4, obj.getUserId());
            stmt.setString(5, obj.getStatus());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Check_Time update(Check_Time obj) {
                String sql = "UPDATE check_time" + " SET check_in = ?, check_out = ?, check_workhour = ?, user_id = ? ,check_pay = ?" + " WHERE check_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            stmt.setString(1, df.format(obj.getIn()));
            stmt.setString(2, df.format(obj.getOut()));
            stmt.setInt(3, obj.getHour());
            stmt.setInt(4, obj.getUserId());
            stmt.setString(4, obj.getStatus());
            stmt.setInt(5, obj.getId());

            stmt.executeUpdate();
            int ret = stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Check_Time obj) {
        String sql = "DELETE FROM check_time WHERE check_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<Check_Time> getAll(String where, String order) {
                ArrayList<Check_Time> list = new ArrayList();
        String sql = "SELECT * FROM check_time where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Check_Time check_time = Check_Time.fromRS(rs);
                list.add(check_time);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
        public int updateHour(int id) {
        String sql = "UPDATE check_time\n" +
"SET check_out = (datetime('now','localtime')),\n" +
"    check_workHour = CASE\n" +
"        WHEN ROUND((julianday((datetime('now','localtime'))) - julianday(check_in))*24.0) > 8 THEN 8\n" +
"        ELSE ROUND((julianday((datetime('now','localtime'))) - julianday(check_in))*24.0)\n" +
"    END\n" +
"WHERE check_id = ?;";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            int rowsUpdated = stmt.executeUpdate();
            return rowsUpdated;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return -1;
        }
    }
       
}
