/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pattarapon
 */
public class MaterialReport {
    private String name;
    private int min;
    private int qty;

    public MaterialReport(String name, int min, int qty) {
        this.name = name;
        this.min = min;
        this.qty = qty;
    }
    public MaterialReport() {
        this.name = "";
        this.min = 0;
        this.qty = 0;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    @Override
    public String toString() {
        return "MaterailReport{" + "name=" + name + ", min=" + min + ", qty=" + qty + '}';
    }
    public static MaterialReport fromRS(ResultSet rs) {
        MaterialReport productReport = new MaterialReport();
        try {
            productReport.setName(rs.getString("material_name"));
            productReport.setMin(rs.getInt("material_min"));
            productReport.setQty(rs.getInt("material_qty"));
            
            
        } catch (SQLException ex) {
            Logger.getLogger(ProductReport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return productReport;
    }
}
