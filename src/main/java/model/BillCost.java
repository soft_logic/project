/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author meepo
 */
public class BillCost {
    private int billID;
    private int billTypeID;
    private Date billDate;
    private double billCost;

    public BillCost(int billID, int billTypeID, Date billDate, double billCost) {
        this.billID = billID;
        this.billTypeID = billTypeID;
        this.billDate = billDate;
        this.billCost = billCost;
    }

    public BillCost(int billTypeID, Date billDate, double billCost) {
        this.billID = -1;
        this.billTypeID = billTypeID;
        this.billDate = billDate;
        this.billCost = billCost;
    }

    public BillCost() {
        this.billID = -1;
        this.billTypeID = -1;
        this.billDate =  null;
        this.billCost = 0.0;
    }

    public int getBillID() {
        return billID;
    }

    public void setBillID(int billID) {
        this.billID = billID;
    }

    public int getBillTypeID() {
        return billTypeID;
    }

    public void setBillTypeID(int billTypeID) {
        this.billTypeID = billTypeID;
    }

    public Date getBillDate() {
        return billDate;
    }

    public void setBillDate(Date billDate) {
        this.billDate = billDate;
    }

    public double getBillCost() {
        return billCost;
    }

    public void setBillCost(double billCost) {
        this.billCost = billCost;
    }

    @Override
    public String toString() {
        return "BillCost{" + "billID=" + billID + ", billTypeID=" + billTypeID + ", billDate=" + billDate + ", billCost=" + billCost + '}';
    }

   
    
    
    public static BillCost fromRS(ResultSet rs) {
        BillCost billcost = new BillCost();
        try {
            billcost.setBillID(rs.getInt("bill_id"));
            billcost.setBillTypeID(rs.getInt("bill_typeid"));
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            String period = rs.getString("bill_date");
            try {
                billcost.setBillDate(df.parse(period));
            } catch (ParseException ex) {
                Logger.getLogger(BillCost.class.getName()).log(Level.SEVERE, null, ex);
            }
            billcost.setBillCost(rs.getInt("bill_cost"));
            
        } catch (SQLException ex) {
            Logger.getLogger(BillCost.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return billcost;
    }
}
