/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tncpop
 */
public class Salary {
     private int salaryid;
    private Date date;
    private double amount;
    private int employeeId;

    public Salary(int salaryid, Date date, double amount, int employeeId) {
        this.salaryid = salaryid;
        this.date = date;
        this.amount = amount;
        this.employeeId = employeeId;
    }

    public Salary(Date date, double amount, int employeeId) {
        this.salaryid=-1;
        this.date = date;
        this.amount = amount;
        this.employeeId = employeeId;
    }

    public Salary() {
        this.salaryid=-1;
    }

    public int getSalaryid() {
        return salaryid;
    }

    public void setSalaryid(int salaryid) {
        this.salaryid = salaryid;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    @Override
    public String toString() {
        return "Salary{" + "salaryid=" + salaryid + ", date=" + date + ", amount=" + amount + ", employeeId=" + employeeId + '}';
    }
    
    public static Salary fromRS(ResultSet rs) {
        Salary salary = new Salary();
        try {
            salary.setSalaryid(rs.getInt("salary_id"));
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            String date = rs.getString("salary_date");
            try {
                salary.setDate(df.parse(date));
            } catch (ParseException ex) {
                Logger.getLogger(Salary.class.getName()).log(Level.SEVERE, null, ex);
            }
            salary.setAmount(rs.getDouble("salary_amount"));
            salary.setEmployeeId(rs.getInt("emp_id"));
        } catch (SQLException ex) {
            Logger.getLogger(Salary.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return salary;
    }
    
}
