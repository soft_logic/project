/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pattarapon
 */
public class ProductReport {
    private String name;
    private float price;
    private int totalQty;
    private float totalPrice;

    public ProductReport(String name, float price, int totalQty, float totalPrice) {
        this.name = name;
        this.price = price;
        this.totalQty = totalQty;
        this.totalPrice = totalPrice;
    }
    public ProductReport() {
        this.name = "";
        this.price = 0;
        this.totalQty = 0;
        this.totalPrice = 0;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Override
    public String toString() {
        return "ProductReport{" + "name=" + name + ", price=" + price + ", totalQty=" + totalQty + ", totalPrice=" + totalPrice + '}';
    }
    public static ProductReport fromRS(ResultSet rs) {
        ProductReport productReport = new ProductReport();
        try {
            productReport.setName(rs.getString("product_name"));
            productReport.setPrice(rs.getFloat("product_price"));
            productReport.setTotalQty(rs.getInt("totalQty"));
            productReport.setTotalPrice(rs.getFloat("totalPrice"));
        } catch (SQLException ex) {
            Logger.getLogger(ProductReport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return productReport;
    }
}
