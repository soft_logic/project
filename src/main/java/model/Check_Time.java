/*
 * Click nbfs://nbhost/SystemFileSystem/Tuserlates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Tuserlates/Classes/Class.java to edit this tuserlate
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Acer
 */
public class Check_Time {
    private int id;
    private Date in;
    private Date out;
    private int hour;
    private int user_id;
    private String status;

    public Check_Time(int id, Date in, Date out, int hour, int user_id,String status) {
        this.id = id;
        this.in = in;
        this.out = out;
        this.hour = hour;
        this.user_id = user_id;
        this.status = status;
    }

    public Check_Time(Date in, Date out, int hour, int user_id,String status) {
        this.id = -1;
        this.in = in;
        this.out = out;
        this.hour = hour;
        this.user_id = user_id;
        this.status = status;
    }

    public Check_Time() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public Date getIn() {
        return in;
    }

    public Date getOut() {
        return out;
    }

    public int getHour() {
        return hour;
    }

    public int getUserId() {
        return user_id;
    }

    public void setIn(Date in) {
        this.in = in;
    }

    public void setOut(Date out) {
        this.out = out;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setUserId(int user_id) {
        this.user_id = user_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Check_Time{" + "id=" + id + ", in=" + in + ", out=" + out + ", hour=" + hour + ", user_id=" + user_id + ", status=" + status + '}';
    }

   

   
    
    
    
    public static Check_Time fromRS(ResultSet rs) {
       Check_Time checktime = new Check_Time();
        try {
            checktime.setId(rs.getInt("check_id"));
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            String in = rs.getString("check_in");
            String out = rs.getString("check_out");
            try {
                checktime.setIn(df.parse(in));
                checktime.setOut(df.parse(out));
            } catch (ParseException ex) {
                Logger.getLogger(Check_Time.class.getName()).log(Level.SEVERE, null, ex);
            }
            checktime.setHour(rs.getInt("check_workHour"));
            checktime.setUserId(rs.getInt("user_id"));
            checktime.setStatus(rs.getString("check_pay"));
        } catch (SQLException ex) {
            Logger.getLogger(Check_Time.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checktime;
    }
}
