/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import Dao.UserDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tncpop
 */
public class Employee {
 private int id;
 private String name;
 private String tel;
 private String email;
 private double workhours;
 private int Userid;
 private User user;

    public Employee(int id, String name, String tel, String email, double workhours, int Userid) {
        this.id = id;
        this.name = name;
        this.tel = tel;
        this.email = email;
        this.workhours = workhours;
        this.Userid = Userid;
        
    }

    public Employee(String name, String tel, String email, double workhours, int Userid) {
        this.id=-1;
        this.name = name;
        this.tel = tel;
        this.email = email;
        this.workhours = workhours;
        this.Userid = Userid;
        
    }

    public Employee() {
     this.id=-1;
        this.name = "";
        this.tel = "";
        this.email = "";
        this.workhours = 0;
        this.Userid = 0;
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getWorkhours() {
        return workhours;
    }

    public void setWorkhours(double workhours) {
        this.workhours = workhours;
    }

    public int getUserid() {
        return Userid;
    }

    public void setUserid(int Userid) {
        this.Userid = Userid;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", name=" + name + ", tel=" + tel + ", email=" + email + ", workhours=" + workhours + ", Userid=" + Userid + ", user=" + user + '}';
    }

   public static Employee fromRS(ResultSet rs) {
        Employee employee = new Employee();
        try {
            employee.setId(rs.getInt("emp_id"));
            employee.setName(rs.getString("emp_name"));
            employee.setTel(rs.getString("emp_tel"));
            employee.setEmail(rs.getString("emp_email"));
            employee.setWorkhours(rs.getDouble("emp_workhours"));
            employee.setUserid(rs.getInt("user_id"));
            //populate
            UserDao userDao = new UserDao();
            User user = userDao.get(employee.getUserid());
            employee.setUser(user);
        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return employee;
    }
 
}
